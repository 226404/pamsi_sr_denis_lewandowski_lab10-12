// KIK.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "Kolko_i_krzyzyk.h"

using namespace std;

int main()
{
	bool powtorzenie = false;
	char c;
	char gracz = 'O';


	do {
		Kolko_i_krzyzyk kik;
		kik.rysuj_plansze();

		while (true)
		{
			kik.ruch(gracz);
			if (kik.remis())
			{
				cout << "REMIS !!!" << endl;
				break;
			}
			if (kik.wygrana('O'))
			{
				cout << "Wygral gracz 'O' !!!" << endl;
				break;
			}
			if (kik.wygrana('X'))
			{
				cout << "Wygral gracz 'X' !!!" << endl;
				break;
			}
		}

		cout << endl << "Czy chcesz powtorzyc gre? (y/n)" << endl;
		cin >> c;
		if (c == 'y') powtorzenie = true;
		else powtorzenie = false;
	}
	while (powtorzenie);

	getchar();
    return 0;
}

