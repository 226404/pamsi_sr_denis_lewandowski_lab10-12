#pragma once
#include <iostream>
#include <cmath>

#define ROZMIAR_PLANSZY 4

using namespace std;


class Kolko_i_krzyzyk
{
	char plansza[ROZMIAR_PLANSZY][ROZMIAR_PLANSZY];		// Przechowuje plansze ( znaki X, O )

public:
	Kolko_i_krzyzyk();
	void rysuj_plansze();
	void ruch(char &gracz);
	int komputer();
	bool remis();
	bool wygrana(char gracz);
	int minimax(char gracz, int glebokosc, int alfa, int beta);
	bool dwa_obok_siebie(char gracz);

	int minimax2(char gracz, int glebokosc, int alfa, int beta);

};


Kolko_i_krzyzyk::Kolko_i_krzyzyk()
{
	for (int i = 0; i < ROZMIAR_PLANSZY; i++)
		for(int j=0; j <ROZMIAR_PLANSZY; j++)
		plansza[i][j] = ' ';
}

void Kolko_i_krzyzyk::rysuj_plansze()
{
	system("cls");
	cout << "Kolko i krzyzyk!" << endl;
	cout << "Zdabadz trzy 'O' w pionie/poziomie/skosie!" << endl;

	for (int i = 0; i < ROZMIAR_PLANSZY; i++)
	{
		for (int j = 0; j < (ROZMIAR_PLANSZY); j++)		// Pionowe kreski
		{
			cout << " " << plansza[i][j];
			if(j != (ROZMIAR_PLANSZY-1)) cout << " |";
		}
		cout << endl;
		
		if (i != (ROZMIAR_PLANSZY - 1))						// Poziome kreski
		for (int j = 0; j < ROZMIAR_PLANSZY; j++)
		{
			cout << "---";
			if (j != (ROZMIAR_PLANSZY - 1))
				cout << "+";
		}
		cout << endl;
	}
}

inline void Kolko_i_krzyzyk::ruch(char &gracz)
{
	int r;

	if (gracz == 'O')   // je�li graczem jest cz�owiek
	{

		cout << endl;
		cout << "Twoj ruch: ";
		do
		{
			cin >> r; // czekamy a� wpisze numer pola
			r--;	  // przesuniecie iteracji ( tablice sa iterowane od 0, chcemy wpisywac pozycje od 1 )
			if (plansza[r/(ROZMIAR_PLANSZY)][r%(ROZMIAR_PLANSZY)] != ' ') cout << "Wybierz ponownie, pole " << (r+1) << " jest zajete!" << endl;
		} while (plansza[r / (ROZMIAR_PLANSZY )][r % (ROZMIAR_PLANSZY )] != ' ');
	}
	else   // je�li graczem jest komputer !!!
	{
		r = komputer();			// dostajemy od komputera info o ruchu jaki wykona�
		cout << endl;
		cout << "Komputer - ruch: " << (r+1) << endl;

	}

		plansza[r/ROZMIAR_PLANSZY][r%ROZMIAR_PLANSZY] = gracz; // oznaczamy na planszy znaczek czyli 0 lub X gracza( komputer lub czlowiek)

	if (gracz == 'O') // zmieniamy gracza na przeciwnego
		gracz = 'X';
	else
		gracz = 'O';

	rysuj_plansze();
}

inline int Kolko_i_krzyzyk::komputer()
{
	int ruch, m, mmx;

	mmx = -10000;			// nieskonczonosc
	
	
	for (int i = 0; i < ROZMIAR_PLANSZY; i++)
		for (int j = 0; j < ROZMIAR_PLANSZY; j++)
		{
			if (plansza[i][j] == ' ')
			{
				plansza[i][j] = 'X';
				m = minimax('X', 5, -INFINITY, INFINITY);
				plansza[i][j] = ' ';
				if (m > mmx)
				{
					mmx = m; ruch = ROZMIAR_PLANSZY*i + j;
				}
			}
		}
	return ruch;
}
inline bool Kolko_i_krzyzyk::remis()
{
	for(int i=0; i<ROZMIAR_PLANSZY*ROZMIAR_PLANSZY; i++)
		if (plansza[i / ROZMIAR_PLANSZY][i%ROZMIAR_PLANSZY] == ' ')
			return false;
		
	return true; 
}

inline bool Kolko_i_krzyzyk::wygrana(char gracz)
{
	for (int j = ROZMIAR_PLANSZY; j >= 3; j--)		// Dla 3x3 petla wykona sie tylko raz
	{
		////// CZY SA 3 W POZIOMIE //////
		for (int i = 0; i < ROZMIAR_PLANSZY; i++)
		{
			if (plansza[i][j - 1] == gracz && plansza[i][j - 2] == gracz && plansza[i][j - 3] == gracz)
			{
				return true;
			}
		}

		///// CZY SA 3 w PIONIE /////////
		for (int i = 0; i < ROZMIAR_PLANSZY; i++)
		{
			if (plansza[j - 1][i] == gracz && plansza[j - 2][i] == gracz && plansza[j - 3][i] == gracz)
			{
				return true;
			}
		}

		///// CZY SA 3 W UKOSIE "rosnacym"  ///////
		for (int i = 0; i < (ROZMIAR_PLANSZY-2); i++)
		{
			if (plansza[j - 1][i] == gracz && plansza[j - 2][i+1] == gracz && plansza[j - 3][i+2] == gracz)
			{
				return true;
			}
		}

		///// CZY SA 3 W UKOSIE "malejacym"  ///////
		for (int i = ( ROZMIAR_PLANSZY - 1); i > 1; i--)
		{
			if (plansza[j - 1][i] == gracz && plansza[j - 2][i - 1] == gracz && plansza[j - 3][i - 2] == gracz)
			{
				return true;
			}
		}


	}
	return false;
}




inline int Kolko_i_krzyzyk::minimax(char gracz, int glebokosc, int alfa, int beta)
{
	int mmx, m;

	if (wygrana(gracz))
		if (gracz == 'O') return -100;			// Jesli wygral czlowiek -100
		else return 100;						// Jesli wygral komputer  100

	if(dwa_obok_siebie(gracz))
		if (gracz == 'O') return -10;			// Jesli 2 znaki czlowieka obok siebie czlowiek -10
		else return 10;						// Jesli 2 znaki komputera obok siebie  10

	

	if (remis() || glebokosc == 0) 
		return 0;							// Jesli remis 0

	if( gracz == 'X' )						// Zmiana gracza 
	gracz = 'O';
	else gracz = 'X';

	if (gracz == 'X')						// Przypisujemy wartosci 10 dla czlowieka lub -10 dla komputera
		mmx = -10;
	else mmx = 10;

	for (int i = 0; i < ROZMIAR_PLANSZY; i++)
		for (int j = 0; j < ROZMIAR_PLANSZY; j++)
			if (plansza[i][j] == ' ')
			{
				plansza[i][j] = gracz;
				m = minimax(gracz, glebokosc-1, -INFINITY, INFINITY);
				plansza[i][j] = ' ';
				if (((gracz == 'O') && (m < mmx)) || ((gracz == 'X') && (m > mmx))) mmx = m;
			}
	return mmx;
}



inline int Kolko_i_krzyzyk::minimax2(char gracz, int glebokosc, int alfa, int beta)
{
	int wynik=0;
	if (remis() || glebokosc == 0)
		return wynik;

	if (wygrana(gracz))
		if (gracz == 'O') return -1;			// Jesli wygral czlowiek -100
		else return 1;						// Jesli wygral komputer  100

//		if (dwa_obok_siebie(gracz))
//			if (gracz == 'O') return -10;			// Jesli 2 znaki czlowieka obok siebie czlowiek -10
//			else return 10;						// Jesli 2 znaki komputera obok siebie  10


			if (gracz == 'X')
			{
				// Znajdz najwiekszy i przechowaj w 'alfa'
				for (int i = 0; i < ROZMIAR_PLANSZY; i++)
					for (int j = 0; j < ROZMIAR_PLANSZY; j++)
						if (plansza[i][j] == ' ')
						{
							plansza[i][j] = gracz;
							wynik = minimax(gracz, glebokosc - 1, -INFINITY, INFINITY);
							plansza[i][j] = ' ';
							if (wynik > alfa) alfa = wynik;
							if (alfa >= beta) break;  // ciecie beta
							return alfa;
						}
			} else 
				for (int i = 0; i < ROZMIAR_PLANSZY; i++)
					for (int j = 0; j < ROZMIAR_PLANSZY; j++)
						if (plansza[i][j] == ' ')
						{
							plansza[i][j] = gracz;
							wynik = minimax(gracz, glebokosc - 1, -INFINITY, INFINITY);
							plansza[i][j] = ' ';
							if (wynik < beta) beta = wynik;
							if (alfa >= beta) break;  // ciecie beta
							return beta;
						}
}


inline bool Kolko_i_krzyzyk::dwa_obok_siebie(char gracz)
{
	for (int j = ROZMIAR_PLANSZY; j >= 3; j--)
	{
		////// CZY SA 2 W POZIOMIE //////
		for (int i = 0; i < ROZMIAR_PLANSZY; i++)
		{
			if (plansza[i][j - 1] == gracz && plansza[i][j - 2] == gracz)
			{
				return true;
			}
		}

		///// CZY SA 2 w PIONIE /////////
		for (int i = 0; i < ROZMIAR_PLANSZY; i++)
		{
			if (plansza[j - 1][i] == gracz && plansza[j - 2][i] == gracz)
			{
				return true;
			}
		}

		///// CZY SA 2 W UKOSIE "rosnacym"  ///////
		for (int i = 0; i < (ROZMIAR_PLANSZY - 2); i++)
		{
			if (plansza[j - 1][i] == gracz && plansza[j - 2][i + 1] == gracz)
			{
				return true;
			}
		}

		///// CZY SA 3 W UKOSIE "malejacym"  ///////
		for (int i = (ROZMIAR_PLANSZY - 1); i > 1; i--)
		{
			if (plansza[j - 1][i] == gracz && plansza[j - 2][i - 1] == gracz)
			{
				return true;
			}
		}


	}
	return false;
}


